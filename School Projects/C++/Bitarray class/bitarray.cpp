/*

BitArray class

Author: Will Hadden
FSU ID: wjh13d
Course: COP 3330 sec. 5
Assignment 7

Summary:
This file contains definitions for the BitArray class, which stores a list of
bits as a dynamic array of unsigned chars. Member functions include constructor,
copy constructor, assignment operator, destructor, insertion operator, equal and
not equal comparison operators, and a function which returns the total number of
bits in the array. Main functions for the class allow the user to Set, Unset, Flip,
and Query each bit in the array
*/


#include "bitarray.h"
#include<iostream>

using namespace std;

const int BITS = (sizeof(char)*8);
//BITS stores the number of bits in each element of
//the array

ostream& operator<< (ostream& os, const BitArray& a)
//insertion operator, prints the bitarray as an unspaced
//list of bits between a set of parentheses
{
   unsigned int l = a.Length();

   os << "(";

   for (int i = 0; i < l; i++)
   {
	if(a.Query(i))
	   os << "1";
	else
	   os << "0";
   }
   os << ")";

   return os;
}

bool operator == (const BitArray& a, const BitArray& b)
//checks if two bitarrays match exactly
{
   if (a.arraySize != b.arraySize)
	return false;

   for (int i = 0; i < a.arraySize; i++)
   {
	if(a.barray != b.barray)
	   return false;
   }

   return true;
}

bool operator!= (const BitArray& a, const BitArray& b)
//checks if two bitarrays dont match exactly
{
   return !(a == b);
}

BitArray::BitArray(unsigned int n)
//creates the smalles array that can hold n bits
//initializes all bits to 0
{
   arraySize = n/BITS;
   if(n%BITS != 0)
	arraySize++;

   barray = new unsigned char[arraySize];

   for (int i = 0; i < arraySize; i++)
	barray[i] = 0;
}

BitArray::BitArray(const BitArray& b)
//copy constructor
{
   arraySize = b.arraySize;
   barray = new unsigned char[arraySize];

   for(int i = 0; i < arraySize; i++)
	barray[i] = b.barray[i];
}

BitArray::~BitArray()
//destructor, cleans dynamically allocated array
{
   delete [] barray;
}

BitArray& BitArray::operator= (const BitArray& a)
//assignment operator
{
   if (this != &a)
   {
	arraySize = a.arraySize;
	barray = new unsigned char[arraySize];

	for(int i = 0; i < arraySize; i++)
	   barray[i] = a.barray[i];
   }
   return *this;
}

unsigned int BitArray::Length() const
// returns the total number of bits in the array
{
   return (arraySize*BITS);
}

void BitArray::Set(unsigned int index)
//sets the desired bit to 1
{
   unsigned char mask = 1;
   mask <<= (index%BITS);

   barray[index/BITS] |= mask;
}

void BitArray::Unset(unsigned int index)
//sets the desired bit to 0
{
   unsigned char mask = 1;
   mask <<= (index%BITS);

   barray[index/BITS] &= ~mask;
}

void BitArray::Flip(unsigned int index)
//inverts the desired bit
{
   unsigned char mask = 1;
   mask <<= (index%BITS);

   barray[index/BITS] ^= mask;
}

bool BitArray::Query(unsigned int index) const
//returns the status of the desired bit
{
   unsigned char temp = barray[index/BITS];
   unsigned char mask = 1;
   mask <<= (index%BITS);
   temp |= mask;

   if(barray[index/BITS] == temp)
	return true;
   else
	return false;
}
