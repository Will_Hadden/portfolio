#include "Vector.h"
#include<iostream>

using namespace std;

template <class T>
class Stack
{
public:
   Stack();
   void push(T&);
   void pop();
   T& top();
   bool empty();
private:
   Vector<T> v;
};

template <class T>
Stack<T>::Stack()
{}
template <class T>
void Stack<T>::push(T& x)
{
   v.push_back(x);
}

template <class T>
void Stack<T>::pop()
{
   if(!empty())
	v.pop_back();
}

template <class T>
T& Stack<T>::top()
{
   if(!empty())
	return v[v.size()-1];
}

template <class T>
bool Stack<T>::empty()
{
   if( v.size() != 0 )
   {
	return false;
   }
   else
   {
	return true;
   }
}

