#ifndef _BST
#define _BST

#include<iostream>
#include<string>

using namespace std;

template <class T>
class Node
{
   public:
	Node();
	~Node();
	void SetValue(const T& val);
	T GetValue();
	T value;
	Node<T> * left, * right;
};

template <class T>
Node<T>::Node()
{
   left = 0;
   right = 0;
}

template <class T>
Node<T>::~Node()
{
   if (left != 0)
	delete left;
   if (right != 0)
	delete right;
}

template <class T>
void Node<T>::SetValue(const T& val)
{
   value = val;
}

template <class T>
T Node<T>::GetValue()
{
   return value;
}

template <class T>
class MRBST
{
   public:
	MRBST();
	~MRBST();
	void push(const T& val);
	bool search(const T& val);
	void PrintPreorder();
   private:
	Node<T> * root;
	bool empty;
	void rotate(Node<T> * axis, Node<T> * parent);
	void RotateRoot();
	void preorder(Node<T> * n);
};

template <class T>
MRBST<T>::MRBST()
{
   root = new Node<T>;
   empty = true;
}

template <class T>
MRBST<T>::~MRBST()
{
   delete root;
}

template <class T>
void MRBST<T>::push(const T& val)
{
   if(empty)
   {
	root->SetValue(val);
	empty = false;
   }
   else
   {
	Node<T> * cur = root;
	bool placed = false;
	while (placed == false)
	{
	   if(val <= cur->value)
	   {
		if (cur->left == 0)
		{
		   cur->left = new Node<T>;
		   cur = cur->left;
		   cur->SetValue(val);
		   placed = true;
		}
		else
		   cur = cur->left;
	   }
	   else if (val > cur->value)
	   {
		if (cur->right == 0)
		{
		   cur->right = new Node<T>;
		   cur = cur->right;
		   cur->SetValue(val);
		   placed = true;
		}
		else
		   cur = cur->right;
	   }
	}
   }
}

template <class T>
bool MRBST<T>::search(const T& val)
{
   Node<T> * last = root;
   Node<T> * cur = root;
   if (cur->value == val)
   {
	RotateRoot();
	return true;
   }
   while(cur != 0)
   {
	if (cur->value == val)
	{
	   rotate(cur, last);
	   return true;
	}
	last = cur;
	if(val < cur->value)
	   cur = cur->left;
	else if(val > cur->value)
	   cur = cur->right;
   }
   return false;
}

template <class T>
void MRBST<T>::RotateRoot()
{
 if(root->right != 0)
 {
   Node<T> * Temp = root->right;
   root->right = Temp->left;
   Temp->left = root;
   root = Temp;
 }
}

template <class T>
void MRBST<T>::rotate(Node<T> * axis, Node<T> * parent)
{
 if(axis->right != 0)
 {
   Node<T> * Temp = axis->right;
   axis->right = Temp->left;
   Temp->left = axis;
   if(parent->left == axis)
	parent->left = Temp;
   else
	parent->right = Temp;
 }
}

template <class T>
void MRBST<T>::preorder(Node<T> * n)
{
   if(n)
   {
	cout << n->value << "\n";
	preorder(n->left);
	preorder(n->right);
   }
}

template <class T>
void MRBST<T>::PrintPreorder()
{
   preorder(root);
}

#endif
