#include "Vector.h"

using namespace std;

template<class T>
class Queue : protected Vector<T>
{
public:
   Queue();
   void push(T&);
   void pop();
   T& front();
   bool empty() const;
private:
   int beg, end;
   void grow();
};

template<class T>
Queue<T>::Queue()
{
   this->capacity = 2;
   this->Size = 0;
   beg = 0;
   end = 0;
   this->array = new T[this->capacity];
}

template<class T>
void Queue<T>::push(T& x)
{
   if(beg == end && this->Size != 0)
	grow();
   this->array[end] = x;
   end++;
   this->Size++;
   if(end == this->capacity)
	end = 0;
}

template<class T>
void Queue<T>::pop()
{
   beg++;
   this->Size--;
   if( beg == this->capacity )
	beg = 0;
}

template<class T>
T& Queue<T>::front()
{
   if (!empty())
	return this->array[beg];
}

template<class T>
bool Queue<T>::empty() const
{
   if( this->Size==0 )
	return true;
   else
	return false;
}

template<class T>
void Queue<T>::grow()
{
   T* temp;
   int tempc = this->capacity;
   this->capacity *= 4;
   temp = new T[this->capacity];
   for(int i = 0; i < this->Size; i++)
   {
	temp[i] = this->array[beg];
	beg++;
	if(beg == tempc)
	   beg = 0;
   }
   delete [] this->array;
   this->array = temp;
   beg = 0;
   end  = tempc;
}
