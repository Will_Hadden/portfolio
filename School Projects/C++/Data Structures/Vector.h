#ifndef _VECTOR
#define _VECTOR

#include<iostream>
#include<iomanip>
using namespace std;

template < class T >
class Vector
{
public:
   Vector();
   Vector<T>( const Vector<T>& orig );
   ~Vector();
   void push_back( const T& e );
   int size() const;
   void pop_back();
   T& operator[] ( int index );
   Vector<T>& operator = (const Vector<T>& v);
protected:
   T* array;
   unsigned int capacity, Size;
   void grow();
};

template < class T >
Vector<T>::Vector()
{
   capacity = 2;
   Size = 0;
   array = new T[capacity];
}

template < class T >
Vector<T>::Vector(const Vector<T>& orig)
{
   capacity = orig.capacity;
   Size = orig.size;
   array = new T[capacity];
   for(int i=0; i < Size; i++)
	array[i] = orig.array[i];
}

template < class T >
Vector<T>::~Vector()
{
   delete [] array;
}

template < class T >
void Vector<T>::grow()
{
   T * temp;
   capacity << 2;
   temp = new T[capacity];
   for(int i=0; i < Size; i++)
	temp[i] = array[i];
   delete [] array;
   array = temp;
}

template < class T >
void Vector<T>::push_back( const T& e )
{
   if(capacity == Size)
	grow();
   array[Size] = e;
   Size++;
}

template < class T >
int Vector<T>::size() const
{
   return Size;
}

template < class T >
void Vector<T>::pop_back()
{
   Size--;
}

template < class T >
T& Vector<T>::operator[] (int index)
{
   return array[index];
}

template < class T >
Vector<T>& Vector<T>::operator = (const Vector<T>& v)
{
   if(this != &v)
   {
	Size = v.size;
	capacity = v.capacity;
	array = new T[capacity];
	for(int i = 0; i < size; i++)
	   array[i] = v.array[i];
   }
   return *this;
}


#endif
