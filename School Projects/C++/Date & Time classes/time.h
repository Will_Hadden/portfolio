/*

Time Class

Author: Will Hadden
FSU ID: wjh13d
Course: COP 3330 sec. 5
Assignment 3

Summary:
This file contains declarations for the time class, which
creates objects representing times in days~hh:mm:ss format.
Member variables are the number of days, hours, minutes, and
seconds. Functions include operator overloads for addition,
subtraction, insertion, extraction, all 6 comparative operators,
as well as increment and decrement(pre and postfix for both.)
*/

#include<iostream>
using namespace std;

class Time
{

friend ostream& operator<< (ostream&, const Time&);
friend istream& operator>> (istream&, Time&);

friend Time operator+ (const Time&, const Time&);
friend Time operator- (const Time&, const Time&);

friend bool operator== (const Time&, const Time&);
friend bool operator!= (const Time&, const Time&);
friend bool operator<= (const Time&, const Time&);
friend bool operator>= (const Time&, const Time&);
friend bool operator< (const Time&, const Time&);
friend bool operator> (const Time&, const Time&);

public:

Time();
Time(int);
Time(int, int, int, int);

Time& operator++ ();
Time operator++ (int);
Time& operator-- ();
Time operator-- (int);

private:

int days,
    hours,
    minutes,
    seconds;

bool Valid(int, int, int, int) const;
void Set(int, int, int, int);
int GetSeconds() const;

};
