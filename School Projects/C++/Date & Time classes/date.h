/*

Date Class

Author: Will Hadden
FSU ID: wjh13d
Course: COP 3330 sec. 5
Assignment 2

Summary:
This file contains declarations for the date class, which
creates objects representing dates of the year. Member variables
include the month, day, and year, as well as a char variable to
represent the desired output format. Member functions allow users 
to set the variables (through parameters or cin), retrieve the components
(month, day, year), display the date in different formats, increase the
date by a desired increment, and compare two date objects chronologically.

This class accounts for leap years and allows for Julian date output.
*/


class Date
{
public:
	//constructor
	Date(int m=1, int d=1, int y=2000);

	//set date with cin
	void Input();

	//show formatted date through cout
	void Show();

	//accessors
	int GetMonth();
	int GetDay();
	int GetYear();

	//mutators
	bool Set(int m, int d, int y);
	bool SetFormat(char f);
	void Increment(int numDays=1);

	//compare two date objects for chronological order
	int Compare(const Date& d);

private:
	int day;
	int month;
	int year;
	char format;
};
