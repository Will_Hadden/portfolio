/*
Date Class    

Author: Will Hadden
FSU ID: wjh13d
Course: COP 3330 sec. 5
Assignment 2

Summary:
This file contains definitions for the date class, which    
creates objects representing dates of the year. Member variables    
include the month, day, and year, as well as a char variable to
represent the desired output format. Member functions allow users
to set the variables (through parameters or cin), retrieve the components                    
(month, day, year), display the date in different formats, increase the     
date by a desired increment, and compare two date objects chronologically.

This class accounts for leap years and allows for Julian date output.
*/


#include<iostream>
#include<iomanip>
#include"date.h"

using namespace std;

const int WIDTH=2;     //
const int JWIDTH=3;   // For formatted output
const char FILL='0'; //

Date::Date(int m, int d, int y)
// constructor, defaults to 1/1/2000
{
	if(!Set(m, d, y))
	{
	  Set(1, 1, 2000);
	}
	
	format = 'D';

	return;
}

void Date::Input()
//allows member data changes through cin
//loops until a valid date is entered
{
	int m,
	    d,
	    y;
	char slash;

	cout << "Please enter the date in M/D/Y format: ";
	cin >> m >> slash >> d >> slash >> y;
	cout << "\n";

	while(!Set(m, d, y))
	{
	   cout << "Invalid date, please try again: ";
	   cin >> m >> slash >> d >> slash >> y;
	   cout << "\n";
	}
}

void Date::Show()
//Displays date through cout. Output format
//changes depending on "format" variable
{
	if(format == 'D')
	{
	   cout << month << "/" << day << "/" << year << "\n";
	}
	
	else if (format == 'T')
	{
	   cout << setw(WIDTH) << setfill(FILL) << month << "/";
	   cout << setw(WIDTH) << setfill(FILL) << day << "/";
	   cout << setw(WIDTH) << setfill(FILL) << year % 100 << "\n";
	}

	else if (format == 'L')
	{
	   switch (month)
	   {
		case 1: cout << "Jan ";
		   break;
		case 2: cout << "Feb ";
		   break;
		case 3: cout << "Mar ";
		   break;
		case 4: cout << "Apr ";
		   break;
		case 5: cout << "May ";
		   break;
		case 6: cout << "June ";
		   break;
		case 7: cout << "July ";
		   break;
		case 8: cout << "Aug ";
		   break;
		case 9: cout << "Sept ";
		   break;
		case 10: cout << "Oct ";
		   break;
		case 11: cout << "Nov ";
		   break;
		case 12: cout << "Dec ";
		   break;
	   }
	   cout << day << ", " << year << "\n";
	}

	else if (format == 'J')
	{
	   int jDay = 0;
	
	   cout << setw(WIDTH) << setfill(FILL) << year % 100 << "-";

	   for(int i=1; i < month; i++)
	   {
		if(i == 1 || i == 3 || i == 5 || i == 7 || i == 8 || i == 10)
		{
		   jDay += 31;
		}
		else if (i == 4 || i == 6 || i == 9 || i == 11)
		{
		   jDay += 30;
		}
		else
		{
		   if(year % 400 == 0 || (year % 4 == 0 && year % 100 != 0))
		   {
			jDay += 29;
		   }
		   else
		   {
			jDay += 28;
		   }
		}
	   }
	   jDay += day;

	   cout << setw(JWIDTH) << setfill(FILL) << jDay << "\n";
	}
}

bool Date::Set(int m, int d, int y)
//Changes member data through parameters
//returns false and makes no changes if invalid data is sent
{
	if (y <= 0)
	{
	  return false;
	}

	if (m < 1 || m > 12)
	{
	  return false;
	}

	switch (m)
	{
	   case 1:
	   case 3:
	   case 5:
	   case 7:
	   case 8:
	   case 10:
	   case 12:
		if (d < 1 || d > 31)
		{
		   return false;
		}
		break;
	   
	   case 4:
	   case 6:
	   case 9:
	   case 11:
		if (d < 1 || d > 30)
		{
		   return false;
		}
	 	break;

	   case 2:
		if (y % 400 == 0 || (y % 4 == 0 && y % 100 != 0))
		{
		    if (d < 1 || d > 29)
		    {
			return false;
		    }
		}
		else if (d < 1 || d > 28)
		{
		    return false;
		}
	}

	day = d;
	month = m;
	year = y;
	
	return true;
}

bool Date::SetFormat(char f)
//sets format for Show function through parameters
//returns false and makes no changes if invalid data is sent
{
	if (f == 'D' || f == 'T' || f == 'L' || f == 'J')
	{
	  format = f;
	  return true;
	}
	else
	  return false;
}

int Date::GetMonth()
//returns value for month
{
	return month;
}

int Date::GetDay()
//returns value for day
{
	return day;
}

int Date::GetYear()
//returns value for year
{
	return year;
}

void Date::Increment(int numDays)
//advances the date by numDays
//rolls over to new month or year if end of
//respective cycle is reached
{
	for (int i=0; i < numDays; i++)
	{
	   if(!Set(month, day + 1, year))
	   {
		if (!Set(month + 1, 1, year))
		{
		   Set(1, 1, year + 1);
		}
	   }
	}
}

int Date::Compare(const Date& d)
//compares two date objects
//returns -1 if calling object is an earlier date,
//0 if the two are the same, and 1 if parameter is earlier
{
	if (year < d.year)
	{
	   return -1;
	}
	else if (year > d.year)
	{
	   return 1;
	}
	else
	{
	   if (month < d.month)
	   {
		return -1;
	   }
	   else if (month > d.month)
	   {
		return 1;
	   }
	   else
	   {
		if (day < d.day)
		{
		   return -1;
		}
		else if (day > d.day)
		{
		   return 1;
		}
		else
		{
		   return 0;
		}
	   }
	}
}

