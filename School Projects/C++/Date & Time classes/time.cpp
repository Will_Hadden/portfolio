/*

Time Class

Author: Will Hadden
FSU ID: wjh13d
Course: COP 3330 sec. 5
Assignment 3

Summary:
This file contains definitions for the time class, which
creates objects representing times in days~hh:mm:ss format.
Member variables are the number of days, hours, minutes, and
seconds. Functions include operator overloads for addition,
subtraction, insertion, extraction, all 6 comparative operators,
as well as increment and decrement(pre and postfix for both.)
*/


#include<iostream>
#include "time.h"

using namespace std;

ostream& operator<< (ostream& os, const Time& T)
// overloaded instertion operator, sends time to
// ostream in days~hh:mm:ss format
{
   os << T.days << "~";
   if (T.hours < 10)
	os << "0";
   os << T.hours << ":";
   if (T.minutes < 10)
	os << "0";
   os << T.minutes << ":";
   if (T.seconds < 10)
	os << "0";
   os << T.seconds;
   return os;
}

istream& operator>> (istream& is, Time& T)
//overloaded extraction operator, reads time object
//from istream in days~hh:mm:ss format
{
   char filler;
   int d, h, m, s;

   is >> d >> filler >> h >> filler >> m >> filler >> s;
   if (T.Valid(d, h, m ,s))
	T.Set(d, h, m, s);

   return is;
}

Time operator+ (const Time& t1, const Time& t2)
//overload addition operator, it adds
{
   Time temp;

   temp.Set(t1.days + t2.days, t1.hours + t2. hours, t1.minutes + t2.minutes,
	t1.seconds + t2.seconds);
   return temp;
}

Time operator- (const Time& t1, const Time& t2)
//overloaded subtraction, finds the difference
//between total number of seconds in two objects
{
   int s = t1.GetSeconds() - t2.GetSeconds();
   Time temp;
   if (s > 0)
	temp.Set(0, 0, 0, s);

   return temp;
}

bool operator< (const Time& t1, const Time& t2)
//less than operator
{
   return(t1.GetSeconds() < t2.GetSeconds());
}

bool operator> (const Time& t1, const Time& t2)
//greater than operator
{
   return (t2 < t1);
}

bool operator>= (const Time& t1, const Time& t2)
// greater than or equal to operator
{
   return !(t1<t2);
}

bool operator<= (const Time& t1, const Time& t2)
//less than or equal to
{
   return !(t1>t2);
}

bool operator== (const Time& t1, const Time& t2)
//equal to
{
   return (t1 <= t2 && t1 >= t2);
}

bool operator!= (const Time& t1, const Time& t2)
//and not equal operator
{
   return ( t1 < t2 || t1 > t2);
}

Time::Time()
// default constructor, sets all member variables to 0
{
   Set(0, 0, 0, 0);
}

Time::Time(int s)
//conversion constructor, sets time for number of seconds == parameter
{
   if(Valid(0, 0, 0, s))
	Set(0, 0, 0, s);
   else
	Set(0, 0, 0, 0);
}

Time::Time(int d, int h, int m, int s)
// constructor, sets days, hours, minutes, and seconds to
// respective parameter
{
   if(Valid(d, h, m, s))
	Set(d, h, m, s);
   else
	Set(0, 0, 0, 0);
}

Time& Time::operator++()
//prefix increment operator
{
   Set(days, hours, minutes, seconds+1);
   return *this;
}

Time Time::operator++ (int x)
//postfix increment operator
{
   Time temp = *this;
   Set(days, hours, minutes, seconds+1);
   return temp;
}

Time& Time::operator--()
//prefix decrement operator
{
   int temp = GetSeconds();

   if (temp > 0) 
	Set(0, 0, 0, temp-1);

   return *this;
}

Time Time::operator-- (int x)
// postfix decrement operator
{
   Time temp = *this;
   int s = GetSeconds();

   if (s > 0)
	Set(0,0,0,s-1);

   return temp;
}

bool Time::Valid(int d, int h, int m, int s) const
//private member function, assures that values
// are >= 0 before setting member variables
{
   if( d < 0 || h < 0 || m < 0 || s < 0 )
        return false;
   else
        return true;
}

void Time::Set(int d, int h, int m, int s)
//private member function, distributes parameters
//across member variables so that variables do not
// exceed their limits
{
   if(s >= 60)
   {
	m += s/60;
	seconds = s % 60;
   }
   else
	seconds = s;

   if( m >= 60 )
   {
	h += m/60;
	minutes = m % 60;
   }
   else
	minutes = m;

   if( h >= 24 )
   {
	d += h/24;
	hours = h % 24;
   }
   else
	hours = h;

   days = d;
}

int Time::GetSeconds() const
//private member function, calculates total number
//of seconds in object for easier subtraction and comparison
{
int d = days,
    h = hours,
    m = minutes,
    s = seconds;

   h += d * 24;
   m += h *60;
   s += m * 60;

   return s;
}
