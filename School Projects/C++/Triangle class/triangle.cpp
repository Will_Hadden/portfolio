/*

Triangle Class

Author: Will Hadden
Course: COP 3330 sec. 5
Assignment 1

Summary:
This file contains definitions for the triangle class, which 
creates objects representing equilateral triangles. Member variables
include size of the triangle's side, as well as the border and fill
characters used to draw the triangle. Member functions allow users to        
set the variables, retrieve the size, perimeter, and area of the triangle,
as well as draw it.
*/


#include "triangle.h"
#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

const int PRECISION = 2;

Triangle::Triangle(int s, char b, char f)
//Constructor initializes the object
//without parameters, sets size to 2, border to #, and fill to *
{
	SetSize(s);
	SetFill(f);
	SetBorder(b);
}

int Triangle::GetSize()
//returns the size of the triangle
{
	return size;
}

int Triangle::Perimeter()
//returns the triangle's perimeter
{
	return (size*3);
}

double Triangle::Area()
//calculates and returns the triangle's area
{
	double area, s, h;

	s=size;
	h=sqrt((s*s)-((s/2)*(s/2)));
	area=.5*s*h;

	return area;
}

bool Triangle::Grow()
//increase size by 1 if allowed
{
	if(size<39)
	{
		size++;
		return true;
	}
	else
	return false;
}

bool Triangle::Shrink()
//decreases size by 1 if allowed;
{
	if(size>1)
	{
		size--;
		return true;
	}
	else
	return false;
}

bool Triangle::SetBorder(char b)
//sets border to an allowed character
//defaults to '#' if invalid value is sent
{	
	if((int) b  >= 33 && (int) b <= 126)
	{
		border = b;
		return true;
	}
	else
	{
		border = '#';
		return false;
	}
}

bool Triangle::SetFill(char f)
//sets fill to an allowed character
//defaults to '*' if invalid value is sent
{
	if( int(f) >= 33 && int(f) <= 126)
	{
		fill = f;
		return true;
	}
	else
	{
		fill = '*';
		return false;
	}
}

bool Triangle::SetSize(int s)
// sets size to an allowed number, if the number
// defaults to max or min if number is too high or low
{
	if(s < 1)
	{
		size = 1;
		return false;
	}
	else if (s > 39)
	{
		size = 39;
		return false;
	}
	else
	{
		size = s;
		return true;
	}
}

void Triangle::Draw()
//attemps to recreate the triangle in the console window
{
	for(int i = 1; i < size; i++)
	{
		cout << " ";
	}
	
	cout << border << "\n";
	
	for(int i = 2; i < size; i++)
	{
		for(int j = 0; j < size-i; j++)
		{
			cout << " ";
		}
		cout << border << " ";
	
		for(int j = 0; j < i-2; j++)
		{
			cout << fill << " ";
		}
		
		cout << border << "\n";
	}

	if (size > 1)
	{
		for(int i = 0; i < size; i++)
		{
			cout << border << " ";
		}
	cout << "\n";
	}
}

void Triangle::Summary()
// displays size, perimeter, area and drawing of triangle
{
	cout << "Size of the triangle's side: " << size << "\n";
	cout << "Perimeter of triangle: " << Perimeter() << "\n";
	cout << "Area of triangle: " << setprecision(PRECISION) 
		<< Area() << "\n";

	cout << "Triangle looks like: " <<  "\n\n";
	Draw();
}
