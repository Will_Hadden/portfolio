/*

Triangle Class

Author: Will Hadden
Course: COP 3330 sec. 5
Assignment 1

Summary:
This file contains declarations for the triangle class, which
creates objects representing equilateral triangles. Member variables
include size of the triangle's side, as well as the border and fill
characters used to draw the triangle. Member functions allow users to
set the variables, retrieve the size, perimeter, and area of the triangle,
as well as draw it.
*/

class Triangle
{
	public:

	Triangle(int s=2, char b='#', char f='*');
	//constructor initializes the object with default size 2

	//accessors
	int GetSize();
	int Perimeter();
	double Area();

	//mutators
	bool Grow();	//increase size by 1
	bool Shrink();  //decrease size by 1
	bool SetBorder(char);
	bool SetFill(char);
	bool SetSize(int);

	void Draw();	//draws an approximation of the triangle
	void Summary(); //showsinformation about the triangle

	private:

	int size;
	char border;
	char fill;
};
