import java.awt.Graphics;
import java.awt.Color;
import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JApplet;
import java.awt.Component;
import java.awt.Font;
import javax.swing.BoxLayout;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Yahtzee 
{
    /**
     * @param args the command line arguments
     */  
    private static StartPanel pan; 
    public static JFrame frame = new JFrame("Notzee");
    public static JApplet app = new JApplet();
    
    public static void main(String[] args) 
    {   
        run();
    }
    
    public static void run()
    {
        
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pan = new StartPanel();
        frame.add(pan);
        frame.setSize(500, 300);
        frame.setVisible(true);
    }
    
    public static class StartPanel extends JPanel
    {
        public StartPanel()
        {
            super();
            setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
            JLabel label = new JLabel("Notzee!", SwingConstants.CENTER);
            label.setFont(new Font("Monospaced", 1, 48));
            label.setAlignmentX(Component.CENTER_ALIGNMENT);
            add(label);
            JLabel l2 = new JLabel("Select the number of players: ");
            l2.setFont(new Font("Monospaced",1, 24));
            l2.setAlignmentX(Component.CENTER_ALIGNMENT);
            add(l2);
            
            PlyrButton b0 = new PlyrButton("1", 1);
            b0.setAlignmentX(Component.CENTER_ALIGNMENT);
            add(b0);
            PlyrButton b1 = new PlyrButton("2", 2);
            b1.setAlignmentX(Component.CENTER_ALIGNMENT);
            add(b1);
            PlyrButton b2 = new PlyrButton("3", 3);
            b2.setAlignmentX(Component.CENTER_ALIGNMENT);
            add(b2);
            PlyrButton b3 = new PlyrButton("4", 4);
            b3.setAlignmentX(Component.CENTER_ALIGNMENT);
            add(b3);
        }
        
        @Override
        public void paintComponent(Graphics g)
        {
            super.paintComponent(g);
            setBackground(Color.GREEN);
        }
        
        public static class PlyrButton extends JButton implements ActionListener
        {
            private final int butVal;
            
            public PlyrButton(String txt, int p)
            {
                super(txt);
                butVal = p;
                addActionListener(this);
            }
            
            
            public void actionPerformed(ActionEvent event)
            {
                new GameLoop(butVal, frame);
                pan.setVisible(false);
            }
        }
    }
}
