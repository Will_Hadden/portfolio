import java.util.Arrays;
import java.awt.Color;
import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.Random;

/**
 *
 * @author Will
 */
public class Player 
{
    private Random r = new Random();
    private int[] roll = new int[5];
    private boolean[] keep = new boolean[5];
    private String name;
    public int[] scores = new int[13];
    private JFrame frame;
    private boolean[] used = new boolean[13];
    public int bonus = 0, round, plyr, finScore;
    private GameLoop game;
    private PlayerPanel panel;
    
    public Player(JFrame f, int p, GameLoop g)
    {
        frame = f;
        plyr = p;
        game = g;
    }
    
    public void takeTurn(int r)
    {
        round = r;
        frame.setSize(500, 600);
        if(r==0)
        {
            name = (String)JOptionPane.showInputDialog(frame,
                    "Player " + (plyr+1) + ", please enter your name: \n",
                    "Player"+(plyr+1));
        }
        for(int i = 0; i < roll.length; i++)
        {
            keep[i] = false;
            roll[i] = 0;
        }
        panel = new PlayerPanel();
        frame.add(panel);
        frame.repaint();
    }
    
    public String getName()
    {
        return name;
    }

    public int getBonus()
    {
        return bonus;
    }
    
    private class PlayerPanel extends JPanel
    {
        private String[] scorecats = {"1's", "2's", "3's", "4's", "5's",
            "6's", "3 of a kind", "4 of a kind", "Small straight", "Large straight",
            "Full house", "Yahtzee!", "Chance"};

        public PlayerPanel()
        {
            super();
            setBackground(Color.GREEN);
            setLayout(new BorderLayout());
            JLabel label = new JLabel(name);
            add(label, BorderLayout.NORTH);
            add(getScorecard(), BorderLayout.WEST);
            JButton b = new JButton("Roll");
            b.addActionListener(new RollHandle());
            add(b, BorderLayout.EAST);
            setVisible(true);
        }
        
        private JPanel getRoll()
        {
            JPanel pan = new JPanel();
            pan.setLayout(new GridLayout(5, 1));
            pan.setBackground(Color.GREEN);
            for(int i = 0; i<5; i++)
                pan.add(new DieButton("" + Player.this.roll[i], i));
            return pan;
        }
        
        private JPanel getScorecard()
        {
            JPanel pan = new JPanel();
            pan.setLayout(new GridBagLayout());
            pan.setBackground(Color.GREEN);
            GridBagConstraints c = new GridBagConstraints();
            c.fill = GridBagConstraints.VERTICAL;
            c.anchor = GridBagConstraints.LINE_START;
            c.insets = new Insets(5, 5, 5, 5);
            for(int i = 0; i < 13; i++)
            {   
                c.gridy = i;
                c.gridx = 0;
                if(!used[i])
                    pan.add(new ScoreButton(scorecats[i], i+1), c);
                else
                    pan.add(new JLabel(scorecats[i]), c);
                c.gridx = 1;
                if(!used[i])
                    pan.add(new JLabel("--"), c);
                else
                    pan.add(new JLabel(""+scores[i]), c);
            }
            return pan;
        }
        
        public class ScoreButton extends JButton implements ActionListener
        {
            private int pos;
            
            public ScoreButton(String txt, int p)
            {
                super(txt);
                pos = p;
                addActionListener(this);
            }
            
            public void actionPerformed(ActionEvent e)
            {
                int[] ordered = new int[5];
                System.arraycopy(roll, 0, ordered, 0, 5);
                Arrays.sort(ordered);
                int score = 0;
                if(roll[0] != 0)
                {
                    switch(this.pos)
                    {
                        case(1):
                        case(2):
                        case(3):
                        case(4):
                        case(5):
                        case(6):
                            for(int i=0; i < 5; i++)
                            {
                                if(ordered[i] == pos)
                                    score += pos;
                            }
                            break;
                        case(7): //3 of a kind
                            if(ordered[0] == ordered[2])
                                score = ordered[0]*3 + ordered[3] + ordered[4];
                            else if(ordered[1] == ordered[3])
                                score = ordered[1]*3 + ordered[0] + ordered[4];
                            else if(ordered[2] == ordered[4])
                                score = ordered[2]*3 + ordered[0] + ordered[1];
                            break;
                        case(8): //4 of a kind
                            if(ordered[0] == ordered[3])
                                score = (ordered[0] * 4) + ordered[4];
                            else if(ordered[1] == ordered[4])
                                score = ordered[0] + (ordered[1] * 4);
                            break;
                        case(9): //small straight
                            if(ordered[0] == ordered[3]-3)
                                score = 30;
                            else if(ordered[1] == ordered[4]-3)
                                score = 30;
                            else if(ordered[0] == ordered[4]-3)
                                score = 30;
                            break;
                        case(10): //large straight
                            if(ordered[0] == ordered[4]-4)
                                score = 40;
                            break;
                        case(11): //full house
                            if(ordered[0] != ordered[4])
                            {
                                if((ordered[0] == ordered[2]) && (ordered[3] == ordered[4]))
                                    score = 25;
                                else if((ordered[2] == ordered[4]) && (ordered[1] == ordered[0]))
                                    score = 25;
                            }
                            break;
                        case(12):
                            if(ordered[0] == ordered[4])
                                score = 50;//Yahtzee
                            break;
                        case(13):
                            for(int i = 0; i < 5; i++)
                                score += ordered[i]; //chance
                            break;
                    }
                }
                if(!used[pos-1])
                {
                int con = JOptionPane.showConfirmDialog(frame, "" + scorecats[pos - 1] +
                        " will earn you " + score + ". Do you want to accept"
                        + " this score?", "Score Confirmation", 
                        JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                if(con == JOptionPane.YES_OPTION)
                {
                    if((ordered[0] == ordered[4]) && (scores[11] != 0))
                        bonus++;
                    scores[pos -1] = score;
                    used[pos - 1] = true;
                    game.nextTurn(round, plyr+1);
                    PlayerPanel.this.setVisible(false);
                }
                }
                else
                {
                    JOptionPane.showMessageDialog(frame, "That category has"
                            + " already been used, please select another.");
                }
            }
        }
        
        public class DieButton extends JButton implements ActionListener
        {
            private int index;
            
            public DieButton(String txt, int i)
            {
                super(txt);
                index = i;
                addActionListener(this);
            }
            
            public void actionPerformed(ActionEvent e)
            {
                keep[index] = !keep[index];
                if(keep[index] == true)
                    setBackground(Color.YELLOW);
                else
                    setBackground(new JButton().getBackground());
            }
        }
        
        public class ReButton extends JButton implements ActionListener
        {
            private int rollcount;
            
            public ReButton(int roll)
            {
                super("Reroll");
                rollcount = roll;
                addActionListener(this);
            }
            
            public void actionPerformed(ActionEvent e)
            {
                for(int i = 0; i < 5; i++)
                {
                    if(!Player.this.keep[i])
                        Player.this.roll[i] = Player.this.r.nextInt(6) + 1;
                    keep[i] = false;
                }
                BorderLayout lay = (BorderLayout)PlayerPanel.this.getLayout();
                PlayerPanel.this.remove(lay.getLayoutComponent(BorderLayout.CENTER));
                PlayerPanel.this.add(getRoll(), BorderLayout.CENTER);
                PlayerPanel.this.remove(lay.getLayoutComponent(BorderLayout.EAST));
                if(rollcount == 2)
                    PlayerPanel.this.add(new ReButton(3), BorderLayout.EAST);
                PlayerPanel.this.revalidate();
                PlayerPanel.this.repaint();
            }
        }
        
        public class RollHandle implements ActionListener
        {
            public void actionPerformed(ActionEvent e)
            {
                for(int i = 0; i < roll.length; i++)
                    Player.this.roll[i] = Player.this.r.nextInt(6) + 1;
                BorderLayout lay = (BorderLayout)PlayerPanel.this.getLayout();
                PlayerPanel.this.remove(lay.getLayoutComponent(BorderLayout.EAST));
                PlayerPanel.this.add(new ReButton(2), BorderLayout.EAST);                
                PlayerPanel.this.add(getRoll(), BorderLayout.CENTER);
                PlayerPanel.this.remove(lay.getLayoutComponent(BorderLayout.NORTH));
                PlayerPanel.this.add(new JLabel("<html>" + name + ", Click the"
                        + " values you want to keep, and click reroll,"
                        + " or select a scoring category to accept this"
                        + " roll.</html>"), BorderLayout.NORTH);
                PlayerPanel.this.revalidate();
                PlayerPanel.this.repaint();
            }
        }
    }
}


