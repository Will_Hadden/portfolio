import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JApplet;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

/**
 *
 * @author Will
 */
public class GameLoop 
{
    private JFrame frame;
    private Player[] plarray;
    private int numPlayers;
    private JPanel fin;
    
    public GameLoop(int p, JFrame f)
    {
        frame = f;
        numPlayers = p;
        plarray = new Player[p];
        for (int i = 0; i < p; i++)
            plarray[i] = new Player(frame, i, this);
        startGame();
    }
    
    private void startGame()
    {
        plarray[0].takeTurn(0);
    }

    public void nextTurn(int r, int p)
    {
        if(p >= numPlayers)
        {
            p = p%numPlayers;
            r++;
        }
        if(r > 12)
            endGame();
        else
            plarray[p].takeTurn(r);
    }
    
    private void endGame()
    {
        frame.setSize(500, 725);
        JPanel endpan = new JPanel(), inpan = new JPanel();
        endpan.setBackground(Color.GREEN);
        endpan.setLayout(new BorderLayout());
        endpan.add(new NewButton(), BorderLayout.NORTH);
        inpan.setLayout(new GridLayout(0,2));
        inpan.setBackground(Color.GREEN);
        for(int i = 0; i < numPlayers; i++)
            inpan.add(new ScorePan(i));
        endpan.add(inpan, BorderLayout.CENTER);
        fin = endpan;
        frame.add(endpan);
        int winner = 0, high = 0;
        for(int i = 0; i < numPlayers; i++)
        {
            if(plarray[i].finScore > high)
            {
                high = plarray[i].finScore;
                winner = i;
            }
        }
        JOptionPane.showMessageDialog(frame, plarray[winner].getName() +
                " wins with a score of " + plarray[winner].finScore);
    }
    
    private class NewButton extends JButton implements ActionListener
    {
        public NewButton()
        {
            super("New Game");
            addActionListener(this);
        }
            
        public void actionPerformed(ActionEvent e)
        {   
            setVisible(false);
            fin.setVisible(false);
            
			frame.removeAll();
                        frame.revalidate();
			frame.repaint();
			Yahtzee.run();
        }
    }
    
    private class ScorePan extends JPanel
    {
        private final String[] scorecats = {"1's", "2's", "3's", "4's", "5's",
            "6's", "3 of a kind", "4 of a kind", "Small straight", "Large straight",
            "Full house", "Yahtzee!", "Chance"};

        public ScorePan(int p)
        {
            super();
            setBackground(Color.GREEN);
            setBorder(new TitledBorder(new LineBorder(Color.black, 5),
                plarray[p].getName()));
            add(getScorecard(p));
        }
        
        private JPanel getScorecard(int p)
        {
            int total=0;
            JPanel pan = new JPanel();
            pan.setLayout(new GridBagLayout());
            pan.setBackground(Color.GREEN);
            GridBagConstraints c = new GridBagConstraints();
            c.fill = GridBagConstraints.VERTICAL;
            c.anchor = GridBagConstraints.LINE_START;
            c.insets = new Insets(0, 5, 0 , 5);
            for(int i = 0; i < 6; i++)
            {   
                c.gridy = i;
                c.gridx = 0;
                pan.add(new JLabel(scorecats[i]), c);
                c.gridx = 1;
                pan.add(new JLabel(""+plarray[p].scores[i]), c);
                total += plarray[p].scores[i];
            }
            c.gridx = 0;
            c.gridy = 6;
            pan.add(new JLabel("Upper Section Bonus"), c);
            c.gridx = 1;
            if(total >= 63)
            {
                pan.add(new JLabel("" + 35), c);
                total += 35;                
            }
            else
                pan.add(new JLabel(""+0), c);
            for(int i = 7; i < 14; i++)
            {   
                c.gridy = i;
                c.gridx = 0;
                pan.add(new JLabel(scorecats[i-1]), c);
                c.gridx = 1;
                pan.add(new JLabel(""+plarray[p].scores[i-1]), c);
                total += plarray[p].scores[i-1];
            }
            c.gridx = 0;
            c.gridy = 14;
            pan.add(new JLabel("Bonus Yahtzees: " + plarray[p].getBonus()), c);
            c.gridx = 1;
            pan.add(new JLabel("" + plarray[p].getBonus()*100), c);
            c.gridx = 0;
            c.gridy = 15;
            pan.add(new JLabel("Total"), c);
            c.gridx = 1;
            pan.add(new JLabel(""+total), c);
            plarray[p].finScore = total;
            return pan;
        }
    }
}
