import java.util.Scanner;
import java.util.Random;

public class TicTacToe
{
   static char[][] board = {{' ', ' ', ' '}, {' ', ' ', ' '},
	 {' ', ' ', ' '}};
   static Player p1, p2;
   static int winner = 0;

   public static void main(String[] args)
   {
	initialize(args);
	for (int i = 0; i < 9; i++)
	{
	   if( i%2 == 0 )
		p1.taketurn(1, p2);
	   else
		p2.taketurn(2, p1);
	   if ( winner != 0 )
		break;
	}
	Player.drawBoard();
	declare(winner);
   }

   public static void initialize(String[] args)
   {
	if (args.length == 0)
	{
	   p1 = new HumanPlayer('X');
	   p2 = new HumanPlayer('O');
	}
	else if (args.length == 1 && args[0].compareTo("-c") == 0)
	{
	   p1 = new CompPlayer('X');
	   p2 = new CompPlayer('O');
	}
	else if (args.length == 2 && args[0].compareTo("-c") == 0)
	{
	   if (args[1].compareTo("-a") == 0)
	   {
		p1 = new AdvancedComp('X');
		p2 = new AdvancedComp('O');
	   }
	   else if (args[1].compareTo("1") == 0)
	   {
		p1 = new CompPlayer('X');
		p2 = new HumanPlayer('O');
	   }
	   else if (args[1].compareTo("2") == 0)
	   {
		p1 = new HumanPlayer('X');
		p2 = new CompPlayer('O');
	   }
	   else
		System.out.println("Usage: TicTacToe [-c [1|2] [-a]]");
	}
	else if (args.length == 3 && args[0].compareTo("-c") == 0 && 
		args[2].compareTo("-a") == 0)
	{
	   if (args[1].compareTo("1") == 0)
	   {
		p1 = new AdvancedComp('X');
		p2 = new HumanPlayer('O');
	   }
	   else if (args[1].compareTo("2") == 0)
	   {
		p1 = new HumanPlayer('X');
		p2 = new AdvancedComp('O');
	   }
	   else
		System.out.println("Usage: TicTacToe [-c [1|2] [-a]]");
	}
	else
	   System.out.println("Usage: TicTacToe [-c [1|2] [-a]]");

   }
   
   public static void declare(int w)
   {
	if (w == 0)
	   System.out.println("Draw");
	else
	   System.out.println("Player " + w + " wins!");
   }

   private static abstract class Player
   {
	public boolean[] moveLog = new boolean[10];
	public int moveCount = 0;
	public abstract void taketurn(int p, Player opp);
	public char marker;

	public Player(char m) {marker=m;}

	public static void drawBoard()
	{
	   System.out.println("\n " + board[0][0] + " | " + board[0][1] +
		" | " + board[0][2] + " \t 1 | 2 | 3 ");
	   System.out.println("-----------\t-----------");
	   System.out.println(" " + board[1][0] + " | " + board[1][1] +
		" | " + board[1][2] + " \t 4 | 5 | 6 ");
	   System.out.println("-----------\t-----------");
	   System.out.println(" " + board[2][0] + " | " + board[2][1] +
		" | " + board[2][2] + " \t 7 | 8 | 9 ");
	}
	
	protected boolean checkWin(int move)
	{
	   int row = (move-1)/3;
	   int col = (move-1)%3;
	   switch(col)
	   {
		case 0:
		   if(board[row][1] == marker && board[row][2] == marker)
			return true;
		   break;
		case 1:
		   if(board[row][0] == marker && board[row][2] == marker)
			return true;
		   break;
		case 2:
		   if(board[row][1] == marker && board[row][0] == marker)
			return true;
		   break;
	   }
	   switch(row)
	   {
		case 0:
		   if(board[1][col] == marker && board[2][col] == marker)
			return true;
		   break;
		case 1:
		   if(board[0][col] == marker && board[2][col] == marker)
			return true;
		   break;
		case 2:
		   if(board[1][col] == marker && board[0][col] == marker)
			return true;
		   break;
	   }
	   if(move == 5)
	   {
		if( board[0][0] == marker && board[2][2] == marker)
		   return true;
		else if(board[0][2] == marker && board[2][0] == marker)
		   return true;
	   }
	   else if(move%2 == 1 && board[1][1] == marker)
	   {
		if(move == 1 && board[2][2] == marker)
		   return true;
		else if(move == 3 && board[2][0] == marker)
		   return true;
		else if(move == 7 && board[0][2] == marker)
		   return true;
		else if(move == 9 && board[0][0] == marker)
		   return true;
	   }
	   return false;
	}
   }

   private static class HumanPlayer extends Player
   {
	Scanner s = new Scanner(System.in);

	public HumanPlayer(char m) {super(m);}

	public void taketurn(int p, Player opp)
	{
	   int move;
	   drawBoard();
	   System.out.print("\nPlayer " + p + 
		", please enter a move(1-9): ");
	   move = s.nextInt();
	   while((move < 1 || move > 9) || 
		board[(move-1)/3][(move-1)%3] != ' ')
	   {
		if(move > 0 && move < 10)
		   System.out.print("That move has already been played," +
			" please enter another move: ");
		else
		   System.out.print("\nInvalid move, " +
			 "please enter a move(1-9): ");
		move = s.nextInt();
	   }
	   board[(move-1)/3][(move-1)%3] = marker;
	   moveLog[move] = true;
	   moveCount++;
	   if(super.checkWin(move)) winner = p;
	}
   }

   private static class CompPlayer extends Player
   {
	protected Random r = new Random();

	public CompPlayer(char m) {super(m);}

	public void taketurn(int p, Player opp)
	{
	   int move = decideMove(opp);
	   drawBoard();
	   System.out.print("\nPlayer " + p + 
		" (computer) chooses position " + move);
	   board[(move-1)/3][(move-1)%3] = marker;
	   moveLog[move] = true;
	   moveCount++;
	   if(checkWin(move)) winner = p;

	}
	
	protected int decideMove(Player opp)
	{
	   if(moveCount > 1)
	   {
	     for(int i = 1; i < 10; i++)
	     {
		if( !moveLog[i] && !opp.moveLog[i] && 
			checkWin(i))
		   return i;
	     }
	   }
	   if(opp.moveCount > 1)
	   {
	     for(int i = 1; i < 10; i++)
	     {
		if( !moveLog[i] && !opp.moveLog[i] && opp.checkWin(i))
		   return i;
	     }
	   }
	   if(board[1][1] == ' ')
		return 5;

	   int lim, count = 0;
	   for(int i = 1; i<10; i++)
	   {
		if (!moveLog[i] && !opp.moveLog[i])
		   count++;
	   }
	   lim = r.nextInt(count);
	   count = 0;
	   for(int i = 1; i<10; i++)
	   {
		if (!moveLog[i] && !opp.moveLog[i] && 
			count == lim)
		   return i;
		else if (!moveLog[i] && !opp.moveLog[i])
		   count++;
	   }
	return 0;
	}
   }

   private static class AdvancedComp extends CompPlayer
   {
	public AdvancedComp(char m) {super(m);}

	protected int decideMove(Player opp)
	{	
	   int[] corners = {1, 3, 7, 9};
	   int[] edges = {2, 4, 6, 8};

	   if(marker == 'X')
	   {
		if(moveCount == 0)
		{
		   return corners[r.nextInt(4)];
		}
		else if(moveCount == 1)
		{
		   if(moveLog[7])
		   {
			if(!opp.moveLog[3])
			   return 3;
			else
			   return 1;
		   }
		   else if(moveLog[1])
		   {
			if(!opp.moveLog[9])
			   return 9;
			else
			   return 3;
		   }
		   else if(moveLog[3])
		   {
			if(!opp.moveLog[7])
			   return 7;
			else
			   return 1;
		   }
		   else if(moveLog[9])
		   {
			if(!opp.moveLog[1])
			   return 1;
			else
			   return 7;
		   }
		}
		else if(moveCount == 2)
		{
		   if(checkBlock(opp) != 0)
			return checkBlock(opp);
		   if(board[2][2] == ' ')
			return 9;
		   else if(board[0][0] == ' ')
			return 1;
		   else if(board[0][2] == ' ')
			return 3;
		   else if(board[2][0] == ' ')
			return 7;
		}
		else
		   return super.decideMove(opp);
	   }
	   else
	   {
		if(opp.moveLog[5])
		{
		   if(moveCount == 0)
			return corners[r.nextInt(4)];
		   else if(moveCount == 1)
		   {
			if(checkBlock(opp) != 0)
			   return checkBlock(opp);
			else
			{
			   if(board[0][0] == ' ')
				return 1;
			   else if(board[0][2] == ' ')
				return 3;
			   else if(board[2][0] == ' ')
				return 7;
			   else if(board[2][2] == ' ')
				return 9;
			}
		   }
		   else
		   {
			if(checkBlock(opp) != 0)
			   return checkBlock(opp);
			return super.decideMove(opp);
		   }
		}
		else
		{
		   if(moveCount == 0)
			return 5;
		   if(moveCount == 1)
		   {			
			if(checkBlock(opp) != 0)
			   return checkBlock(opp);
			if(opp.moveLog[2] && opp.moveLog[8])
			   return 4;
			else if(opp.moveLog[4] && opp.moveLog[6])
			   return 2;
			else if((opp.moveLog[1] && opp.moveLog[9]) ||
				(opp.moveLog[3] && opp.moveLog[7]))
			   return edges[r.nextInt(4)];
			else
			   return super.decideMove(opp);
		   }
		   else if (moveCount == 2)
		   {
			if(opp.moveLog[2] && opp.moveLog[8] &&
				 moveLog[4] && opp.moveLog[6])
			   return 1;
			else if(opp.moveLog[4] && opp.moveLog[6] &&
				 moveLog[2] && opp.moveLog[8])
			   return 3;
			else
			   return super.decideMove(opp);
		   }
		   else
			return super.decideMove(opp);
		}
	   }
	   return 0;
	}

	private int checkBlock(Player opp)
	{
	   for(int i = 1; i < 10; i++)
	   {
		if( !moveLog[i] && !opp.moveLog[i] && opp.checkWin(i))
		   return i;
	   }
	   return 0;
	}
   }
}
